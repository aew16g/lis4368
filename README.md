> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Alexander Wilson

### LIS4368 Requirements:

*Course Work Links:* 

##### 1. [A1 README.md](a1/README.md "My A1 README.md file")
- Install Tomcat
- Install JDK
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutorials (bitbucketstationlocation and myteamquotes)
- provide git command descriptions

##### 2. [A2 README.md](a2/README.md "My A2 README.md file")
- Creation of ebookshop
- creation of query servlet
- displaying ebookshop with query servlet

##### 3. [A3 README.md](a3/README.md "My A3 README.md file")
- Creation of Petstore database
- Forward Engineering of database
- Display Inserts of Tables

##### 4. [A4 README.md](a4/README.md "My A4 README.md file")
- Creation of application
- Creation of failed application
- creation of passed application

##### 5. [A5 README.md](a5/README.md "My A5 README.md file")
- Creation of Forum page
- Insertion of information into MYSQL
##### 6. [P1 README.md](p1/README.md "My P1 README.md file")
- Create data validation page
- create homepage

##### 7. [P2 README.md](p2/README.md "My P2 README.md file")
- Create Table page
- Create Update Page
- Create Deletion Utility

