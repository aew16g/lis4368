package crud.data;

import java.sql.*;
//import java.sql.SQLException;

import javax.sql.DataSource;
//import jdk.internal.agent.ConnectorAddressLink;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConnectionPool
{
    private static ConnectionPool pool = null;
    private static DataSource dataSource = null;

    
private ConnectionPool()
    {
        try
        {
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/aew16g");
        }
        
        catch (NamingException e)
        {
            System.out.println(e);
        }

    }


    public static synchronized ConnectionPool getInstance()
    {
        if (pool == null)
        {
            pool = new ConnectionPool();
        }
        return pool;
    }

    public Connection getConnection()
    {
        try 
        {
            return dataSource.getConnection();
        }
        catch (SQLException e)
        {
            System.out.println(e);
            return null;
        }
    }

    public void freeConnection(Connection c)
    {
        try
        {
            c.close();
        } catch (SQLException e)
        {
            System.out.println(e);
        }
    }

}