> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development
## Alexander Wilson

### Assignment 1 Requirements:

Three Parts:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch 1, 2)

#### README.md file should include the following items:

* Screenshot running java Hello
* Screenshot running Tomcat
* Git commands with short descriptions
* Bitbucket repo links

#### Git commands w/short descriptions:

1. git init - creates new Git repo 
2. git status - displays working directory state as well as staging area
3. git add - add change in working directory to the staging area
4. git commit - saves your changes to the local repo
5. git push - uploads local repo to a remote repo
6. git pull - updates the local version of a repo from a remote
7. git clone - targets an existing repo and creates a clone

#### Assignment Screenshots:




| Screenshot of Java Hello                           |
|----------------------------------------------------|  
|![java Hello](img/java.png)                         |

| Screenshot of Tomcat                               |
|----------------------------------------------------|
|![MyFirst App](img/tomcat.png)                      |

| Screenshot of Localhost                            |
|----------------------------------------------------|
|![Localhost](img/webpage.png)                       |




#### Tutorial Links:

Bitbucket Tutorial:
[A1 BitbucketStationLocations Tutorial](https://bitbucket.org/aew16g/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")
