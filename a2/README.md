
# LIS4368 - Advanced Web Application Development
## Alexander Wilson

### Assignment 2 Requirements:

Two Parts:

1. Create database ebookshop
2. Query ebookshop in Tomcat

#### README.md file should include the following items:

* Assessment links 
* Screenshot of query results



#### Assignment Screenshots:




| Screenshot of Java Hello                           |Screenshot of Hello World!                          |
|----------------------------------------------------|----------------------------------------------------|
|![java Hello](img/hello1.png)                       |![MyFirst App](img/helloworld.png)                  |

| Screenshot of Query                                | Screenshot of Query Result                         |
|----------------------------------------------------|----------------------------------------------------|
|![MyFirst App](img/choice.png)                      |![Localhost](img/query.png)                         |

|                                                Screenshot of SayHi                                      |
|---------------------------------------------------------------------------------------------------------|
|![Localhost](img/sayhi.png)                                                                              |


| Screenshot of Website                              |                                                    |
|----------------------------------------------------|----------------------------------------------------|
|![java Hello](img/website1.png)                     |![MyFirst App](img/website2.png)                    |
|                                                                                                         |
|                                     ![MyFirst App](img/website3.png)                                    |

#### Tutorial Links:

Bitbucket Tutorial:
[A1 BitbucketStationLocations Tutorial](https://bitbucket.org/aew16g/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")
