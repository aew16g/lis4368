
# LIS4368 - Advanced Web Application Development
## Alexander Wilson

### Assignment 3 Requirements:

Two Parts:

1. Create database Petstore
2. Include inserts into tables

#### README.md file should include the following items:
 
* Screenshot data and tables
* links to database files

#### ERD Business Rules: 

 A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:

1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.

2. A store has many pets, but each pet is sold by only one store.


#### Assignment Screenshots:


|                                                Screenshot of ERD                                        |
|---------------------------------------------------------------------------------------------------------|
|![Localhost](img/a3.png)                                                                                 |

| Screenshot of inserts                              |Screenshot of inserts                               |
|----------------------------------------------------|----------------------------------------------------|
|![java Hello](img/inserts1.png)                     |![MyFirst App](img/inserts2.png)                    |

|                                                Screenshot of inserts                                    |
|---------------------------------------------------------------------------------------------------------|
|![Localhost](img/inserts3.png)                                                                           |


|                                                Screenshot of index.jsp                                    |
|---------------------------------------------------------------------------------------------------------|
|![Localhost](img/index.png)                                                                           |

#### Assignment Links:

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

