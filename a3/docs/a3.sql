-- MySQL Script generated by MySQL Workbench
-- Tue Feb 16 18:57:33 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema aew16g
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `aew16g` ;

-- -----------------------------------------------------
-- Schema aew16g
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `aew16g` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `aew16g` ;

-- -----------------------------------------------------
-- Table `aew16g`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aew16g`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `aew16g`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `pst_name` VARCHAR(45) NOT NULL,
  `pst_street` VARCHAR(45) NOT NULL,
  `pst_city` VARCHAR(45) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `aew16g`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aew16g`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `aew16g`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL,
  `cus_fname` VARCHAR(45) NOT NULL,
  `cus_lname` VARCHAR(45) NOT NULL,
  `cus_street` VARCHAR(45) NOT NULL,
  `cus_city` VARCHAR(45) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `aew16g`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aew16g`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `aew16g`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL,
  `pst_id` SMALLINT UNSIGNED NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(45) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `aew16g`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `aew16g`.`petstore` (`pst_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `aew16g`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `aew16g`;
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Humphree', '312 Dillmore', 'Pensacola', 'FL', 6374829473, 32506, 'sdjf@gmail.com', 'Luffy.com', 12345678, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Lewis', '432 Godwood', 'Pensacola', 'FL', 5473821232, 32506, 'Hola@gmail.com', 'Hekko.com', 44242352, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Darling', '412 Tiki', 'Pensacola', 'FL', 8402010431, 32506, 'Yessi@gmail.com', 'Buffy.com', 65234345, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Izzy', '654 Bloopie', 'Pensacola', 'FL', 5434212423, 32506, 'aeasd@gmail.com', 'Tito.com', 34525234, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'Spaz', '563 Stunnin', 'Pensacola', 'FL', 8764425423, 32506, 'saasd@gmail.com', 'Haal.com', 23452134, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'God', '632 Glass', 'Pensacola', 'FL', 7643456342, 32506, 'rwqws@gmail.com', 'Mamo.com', 32673545, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Lefty', '652 W ten', 'Pensacola', 'FL', 4524234545, 32506, 'esgsds@gmail.com', 'Diki.com', 65345445, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'None', '544 Bakoo', 'Pensacola', 'FL', 6431474574, 32506, 'esas@gmail.com', 'Speeda.com', 7435434, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Hurre', '532 Lala', 'Pensacola', 'FL', 7544422423, 32506, 'wsss@gmail.com', 'Hallll.com', 52123423, NULL);
INSERT INTO `aew16g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_zip`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Glaba', '652 Mickal', 'Pensacola', 'FL', 5412743545, 32506, 'wassx@gmail.com', 'ewsd.com', 76324545, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `aew16g`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `aew16g`;
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (100, 'Savannah', 'Snider', '321 SickoMode', 'Pensacola', 'FL', 32506, 4182139172, 'Hardson@gmail.com', 471728, 431212, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (101, 'Hoo', 'Dat', '311 Noone', 'Pensacola', 'FL', 32506, 5718238123, 'Greyson@gmail.com', 431212, 471728, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (102, 'Blanko', 'Brown', '312 Noon', 'Pensacola', 'FL', 32506, 2132242343, '2ese@gmail.com', 231231, 415324, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (103, 'brown', 'Grey', '554 Handsome', 'Pensacola', 'FL', 32506, 3431321221, '121wd@gmail.com', 415324, 231231, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (104, 'semme', 'Gheto', '543 Savannah st', 'Pensacola', 'FL', 32506, 5145432412, 'Hallow@gmail.com', 543223, 231233, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (105, 'auto', 'Letto', '653 Goodson', 'Pensacola', 'FL', 32506, 7543244142, 'Haaaal@gmail.com', 231233, 543223, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (106, 'mattic', 'Gerson', '643 Heller', 'Pensacola', 'FL', 32506, 5124423421, 'GoofyBoy@gmail.com', 123143, 412312, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (107, 'greyson', 'Halla', '411 Old', 'Pensacola', 'FL', 32506, 4541224334, 'Hateithere@gmail.com', 412312, 123143, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (108, 'honest', 'togod', '412 Yellow Rd', 'Pensacola', 'FL', 32506, 3132224134, 'sasds@gmail.com', 231231, 121334, NULL);
INSERT INTO `aew16g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (109, 'precious', 'thang', '542 High st', 'Pensacola', 'FL', 32506, 3432135143, 'yur@gmail.com', 121334, 231231, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `aew16g`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `aew16g`;
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (50, 1, 100, 'Terrier', 'm', 1234, 1234, 10, 'white', '2020-10-05', 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (51, 2, 101, 'Daschund', 'f', 1234, 1234, 1, 'pearl', '2020-10-02', 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (52, 3, 102, 'Hound', 'm', 1234, 1234, 2, 'red', NULL, 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (53, 4, 103, 'Mutt', 'f', 6344, 6344, 5, 'grey', NULL, 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (54, 5, 104, 'dog', 'm', 7452, 7452, 1, 'silver', NULL, 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (55, 6, 105, 'perro', 'f', 4521, 7452, 43, 'black', '2020-10-01', 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (56, 7, 106, 'cat', 'f', 6323, 5234, 31, 'white', NULL, 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (57, 8, 107, 'animal', 'm', 5234, 6323, 53, 'yellow', '2020-10-04', 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (58, 9, 108, 'pigeon', 'm', 2341, 4123, 1, 'blonde', NULL, 'y', 'y', NULL);
INSERT INTO `aew16g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (59, 10, 109, 'salamander', 'f', 4123, 2341, 2, 'blue', '2020-10-01', 'y', 'y', NULL);

COMMIT;

