# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 4 Requirements:

Three Parts:

1. Creation of Application
2. Creation of inputted data
2. Skillsets 10-12 development


#### README.md file should include the following items:

* Screenshot of failed page
* Screenshot of passed page
* Screenshot running Skillsets 10-12


#### Assignment Screenshots:




| Screenshot of unpopulated App                      | Screenshot of Populated App                        |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/failed.png)                         | ![MyFirst App](img/passed.png)                   |

| Screenshot of Skillset 10:                         |    Screenshot of Skillset 11:                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss10.png)               |  ![Second User Interface](img/ss11.png)            |

|                                         Screenshot of Skillset 12:                                      |
|---------------------------------------------------------------------------------------------------------|
|![third skillset](img/ss12.png)                                                                          |

