# LIS4331 - Advanced Mobile Application Development
## Alexander Wilson

### Assignment 5 Requirements:

Three Parts:

1. Creation of Application
2. Creation of inputted data
2. Skillsets 13-15 development


#### README.md file should include the following items:

* Screenshot of Data
* Screenshot of passed Data
* Screenshot running Skillsets 13-15


#### Assignment Screenshots:




| Screenshot of Populated App                        | Screenshot of Information                          |
|----------------------------------------------------|----------------------------------------------------|     
|![java Hello](img/homepage.png)                     | ![MyFirst App](img/results.png)                    |

| Screenshot of Database                             |    Screenshot of Skillset 13:                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/db.png)                 |  ![Second User Interface](img/ss13.png)            |

| Screenshot of Skillset 14:                         |    Screenshot of Skillset 15:                      |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/ss14.png)               |  ![Second User Interface](img/ss15.png)            |

