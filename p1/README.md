> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development
## Alexander Wilson

### Project 1 Requirements:

Three Parts:

1. Create homepage
2. create Failed Validation
3. Create Passed Validation

#### README.md file should include the following items:

* Screenshot homepage
* Screenshot pass/fail validation

#### Assignment Screenshots:




| Screenshot of Homepage                             |
|----------------------------------------------------|  
|![java Hello](img/homepage.png)                     |

| Screenshot of Incorrect Validation                 |
|----------------------------------------------------|
|![MyFirst App](img/incorrect.png)                   |

| Screenshot of Correct Validation                   |
|----------------------------------------------------|
|![Localhost](img/correct.png)                       |

