> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development
## Alexander Wilson

### Project 2 Requirements:

Five Parts:

1. Create homepage
2. create Database Page
3. Create Update Page
4. Show that they both work
5. Allow deletion 

#### README.md file should include the following items:

* Screenshot of data being Inserted, Updated, Deleted
* Screenshot of data in mysql

#### Assignment Screenshots:


| Screenshot of Data being inserted                  | Screenshot of Thanks.jsp                           |
|----------------------------------------------------|----------------------------------------------------|  
|![java Hello](img/first.png)                        |![java Hello](img/second.png)                       |

| Screenshot of table                | Screenshot of Update                        | Screenshot of Database                  |
|------------------------------------|---------------------------------------------|-----------------------------------------| 
|![java Hello](img/third.png)        |![java Hello](img/fourth.png)                |![java Hello](img/database.png)          |

| Screenshot of Updated Table                        | Screenshot of Delete                               |
|----------------------------------------------------|----------------------------------------------------|  
|![java Hello](img/fifth.png)                        |![java Hello](img/last.png)                         |
